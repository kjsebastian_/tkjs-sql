var NI = require('tkjs-ni');

var _driverId = 0;
var _traceProcessSingleQueue = false;
var _traceQuery = false;
var _traceDeadlock = true;

function createDriverMySQL(aInitConfig)/*: tSQLDriver */ {
  var MYSQL = require('mysql');
  var POOL = MYSQL.createPool(NI.shallowClone(
    {
      acquireTimeout: 30000
    },
    aInitConfig));

  var myDriverId = ++_driverId;

  var _autoDestroyConnection = aInitConfig.autoDestroyConnection || 1000;
  var _autoBatchCall = aInitConfig.autoBatch;
  var _queueMaxItems = aInitConfig.autoBatchMaxItems || 50;
  var _maxSimultaneousQueueProcessing = 5;
  var _deadlockRetries = (aInitConfig.deadlockRetries !== undefined) ? aInitConfig.deadlockRetries : 0;
  var _deadlockRetryMinMs = aInitConfig.deadlockRetryMinMs || 100;
  var _deadlockRetryMaxMs = aInitConfig.deadlockRetryMaxMs || 1000;
  if (_deadlockRetries && _autoBatchCall) {
    // _deadlockRetries doesn't work with auto batching.
    throw NI.Error("Can't use autoBatch with deadlockRetries.");
  }

  var _queueId = 1;
  var _queueEmpty = {
    id: -1,
    count: 0,
    query: "",
    values: [],
    onSucceeded: [],
    onError: []
  };

  var _currentQueue;
  function queueReset() {
    var currentQ = _currentQueue;
    _currentQueue = NI.assignShallowClone({},_queueEmpty);
    _currentQueue.id = _queueId++;
    return currentQ;
  }
  queueReset();

  function queueMySQLCall(aQuery,aValues,aOnSucceeded,aOnError) {
    _currentQueue.count += 1;
    _currentQueue.query += aQuery;
    _currentQueue.values = _currentQueue.values.concat(aValues);
    _currentQueue.onSucceeded.push(aOnSucceeded);
    _currentQueue.onError.push(aOnError);
    if (_currentQueue.count == 1) {
      // first item added
      scheduleProcessNextQueue();
    }
    else if (_currentQueue.count >= _queueMaxItems) {
      // too many items in the current queue, we close the current one
      // and move it into _nextQueueToProcess
      _nextQueueToProcess.push(queueReset());
    }
  }

  var _tick = 0;
  var _nextQueueToProcess = [];
  var _scheduledProcessNextQueue = 0;
  function scheduleProcessNextQueue() {
    if (_scheduledProcessNextQueue >= _maxSimultaneousQueueProcessing) {
      return;
    }
    NI.callNextTick(processNextQueue);
    ++_scheduledProcessNextQueue
  }
  function processNextQueue() {
    --_scheduledProcessNextQueue;
    ++_tick;
    if (_nextQueueToProcess.length == 0) {
      if (_currentQueue.count > 0) {
        // process the current queue if that's all that's left to do
        processSingleQueue(queueReset());
      }
    }
    else {
      processSingleQueue(_nextQueueToProcess.shift());
    }
  }

  function processSingleQueue(aQueue) {
    if (aQueue.count == 0) {
      return;
    }
    else if (aQueue.count == 1) {
      doMySQLQuery(aQueue.query, aQueue.values,
                   function(aRes) {
                     aQueue.onSucceeded[0](aRes);
                     scheduleProcessNextQueue();
                   },
                   function(aErr) {
                     aQueue.onError[0](aErr);
                     scheduleProcessNextQueue();
                   });
      return;
    }

    var timerStart = NI.timerInSeconds();

    if (_traceProcessSingleQueue) {
      NI.log("... [%d,%d] START MYSQL.processSingleQueue: %d calls, tick %d: %s",
             myDriverId,
             aQueue.id, aQueue.count, _tick,
             NI.stringBefore(aQueue.query, ";"));
    }

    doMySQLQuery(
      aQueue.query, aQueue.values,
      function(aRes) {
        NI.forEach(aQueue.onSucceeded, function(aOnSucceeded,i) {
          var base = i*2;
          aOnSucceeded([aRes[base+0], aRes[base+1]]);
        });

        if (_traceProcessSingleQueue) {
          NI.log("... [%d,%d] DONE  MYSQL.processSingleQueue: executed in %.3fs, %d calls, tick %d: %s",
                 myDriverId, aQueue.id,
                 NI.timerInSeconds() - timerStart,
                 aQueue.count, _tick,
                 NI.stringBefore(aQueue.query, ";"));
        }
        scheduleProcessNextQueue();
      },
      function(err) {
        NI.forEach(aQueue.onError, function(aOnError) {
          aOnError(err);
        });
        scheduleProcessNextQueue();
      })
  }

	var isMysqlDeadlockError = function(err) {
		if (err) {
      // Regular deadlock: ER_LOCK_DEADLOCK
      if (err.code === 'ER_LOCK_DEADLOCK' &&
          err.errno === 1213 &&
          err.sqlState==='40001') {
        return 1;
      }
      // ER_SP_DOES_NOT_EXIST, side effect of ER_LOCK_DEADLOCK when EXIT HANDLER is used
      if (err.code === 'ER_SP_DOES_NOT_EXIST' &&
          err.errno === 1305 &&
          err.sqlState==='42000') {
        return 2;
      }
    }
    return 0;
	}

  function proxyMysqlDeadlockRetries(aConn,aQueryId) {
	  var retries = _deadlockRetries;
	  var minMillis = _deadlockRetryMinMs;
	  var maxMillis = _deadlockRetryMaxMs;

	  var orig = aConn.query
	  aConn.query = function(sql, params, next) {
		  var r = retries
		  var nextProxy = function(err,a,b,c,d,e,f) {
        var deadlockCode = isMysqlDeadlockError(err);
			  if ((deadlockCode > 0) && --r) {
          if (_traceDeadlock) {
            NI.warn("MYSQL query[id:%d] deadlocked: %d.", aQueryId, deadlockCode);
          }
				  var sleepMillis = Math.floor((Math.random()*maxMillis)+minMillis)
				  setTimeout(function() {
					  orig.apply(aConn, [sql, params, nextProxy])
				  }, sleepMillis)
			  } else {
				  next(err, a, b, c, d, e, f)
			  }
		  }
		  orig.apply(aConn, [sql, params, nextProxy])
	  }
  }

  var _queryCount = 0;
  function doMySQLQuery(aQuery,aValues,aOnSucceeded,aOnError) {
    return POOL.getConnection(function(aConnErr,conn) {
      if (aConnErr) {
        NI.sendError(aOnError,aConnErr,'MYSQL Query CANTCONN');
        return;
      }

      var queryId = ++_queryCount;
      if (_deadlockRetries > 0) {
        proxyMysqlDeadlockRetries(conn, queryId);
      }
      return conn.query(aQuery,aValues,function(err,rows) { // eslint-disable-line
        if (_autoDestroyConnection > 0) {
          conn.__queryCount = conn.__queryCount || 0;
          if (++conn.__queryCount >= _autoDestroyConnection) {
            conn.destroy(); // destroy connection every once in a while for maintenance...
            NI.info("MYSQL connection auto destroyed for maintenance (used %d times).", conn.__queryCount);
          }
          else {
            conn.release(); // put conn back in pool
          }
        }
        else {
          conn.release(); // put conn back in pool
        }

        if (err) {
          NI.sendError(
            aOnError,err,
            NI.format('MYSQL[qid:%d] ERROR: %s, errno: %s, sqlState: %s: query: %s, values: %j',
                      queryId, err.code, err.errno, err.sqlState, aQuery, aValues)
          );
          return;
        }
        if (aOnSucceeded) {
          try {
            aOnSucceeded(rows);
          }
          catch (exc) {
            NI.sendError(aOnError,exc,
                         NI.format('MYSQL[qid:%d] Handler Exception', queryId));
            return;
          }
        }
      });
    });
  }

  return {
    query: function(aQuery,aValues,aOnSucceeded,aOnError) {
      if (_autoBatchCall && NI.stringStartsWith(aQuery,"CALL")) {
        if (_traceQuery) {
          NI.log("... MYSQL.query (auto batched): " + aQuery + " :: %j", aValues);
        }
        queueMySQLCall(aQuery,aValues,aOnSucceeded,aOnError);
      }
      else {
        if (_traceQuery) {
          NI.log("... MYSQL.query: " + aQuery + " :: %j", aValues);
        }
        doMySQLQuery(aQuery,aValues,aOnSucceeded,aOnError);
      }
    },

    buildFunctionCall: function(aFunction, aParams) {
      var r =  "CALL " + aFunction.name + "(";
      if (aParams && aParams.length) {
        var numParams = aParams.length, paramDef;
        for (var i = 0; i < numParams; ++i) {
          r += "?"
          if ((i+1) != numParams) {
            r += ","
          }
          // process parameter types
          paramDef = aFunction.params[i];
          if (paramDef.type == "json") {
            // convert json to text
            aParams[i] = JSON.stringify(aParams[i]);
          }
        }
      }
      r += ");"
      return r;
    },

    handleFunctionCallResult: function(aFunction, aResult, aOnSucceeded, aOnError) {
      try {
        var rows = [];
        var resultType = NI.objectType(aResult);
        // NI.print("... MYSQL handleFunctionCallResult: %s, %K", aFunction.name, aResult);
        if (resultType == "object") {
          if (!("affectedRows" in aResult)) {
            throw NI.format("MySQL '%s': Invalid result object.",aFunction.name);
          }
          if (aOnSucceeded) {
            // Then should just be a 'ok' notification, we do the same as PGSQL
            // with an int function, { "func_name": affectedRows }
            var retObj = {};
            retObj[aFunction.name] = aResult.affectedRows;
            rows.push(retObj);
            aOnSucceeded(rows);
          }
        }
        else if (resultType != "array") {
          NI.sendError(
            aOnError,
            NI.format("MySQL '%s': Unexpected result type '%s'.",aFunction.name,resultType));
        }
        else if (aOnSucceeded) {
          var resultLen = aResult.length;
          var setType, setRows, rowIndex, numRows, key, val, row;
          for (var i = resultLen-1; i >= 0; --i) {
            setRows = aResult[i];
            setType = NI.objectType(setRows);
            if (setType == "array") {
              rows = rows.concat(setRows);
              numRows = setRows.length;
              for (rowIndex = 0; rowIndex < numRows; ++rowIndex) {
                row = setRows[rowIndex];
                for (key in row) {
                  val = row[key];
                  if (typeof(val) === "string") {
                    row[key] = NI.jsonParseMaybe(row[key]);
                  }
                }
              }
            }
            else if (setType == "object") {
              if ("affectedRows" in setRows &&
                  "fieldCount" in setRows)
              {
                // nothing to do... expected object
              }
              else {
                throw NI.format(
                  "MySQL '%s': Unexpected object in MySQL call reply: [%d;%d]\n%K",
                  aFunction.name,
                  i, resultLen,
                  setRows);
              }
            }
            else {
              throw NI.format(
                "MySQL '%s': Unexpected set type: '%s'.",
                aFunction.name,
                setType);
            }
          }
          aOnSucceeded(rows);
        }
      }
      catch (exc) {
        NI.sendError(
          aOnError,exc,
          NI.format("MySQL '%s': Function Result Exception", aFunction.name));
        return;
      }
    },
  }
};
exports.createDriver = createDriverMySQL;
