/* global __DEV__ */
var NI = require('tkjs-ni');

function createDriverPG(aInitConfig)/*: tSQLDriver */ {
  var PGSQL = require('pg');
  NI.assert.has(aInitConfig,'host');
  NI.assert.has(aInitConfig,'user');
  NI.assert.has(aInitConfig,'password');
  NI.assert.has(aInitConfig,'database');
  // var conString = "postgres://" + aInitConfig.user + ":" + aInitConfig.password + "@" + aInitConfig.host + "/" + aInitConfig.database;

  var conConfig = {
    host: aInitConfig.host,
    port: aInitConfig.port,
    user: aInitConfig.user,
    password: aInitConfig.password,
    database: aInitConfig.database,
    poolSize: aInitConfig.connectionLimit || 10
  };

  function doQuery(aClient, aQuery, aValues, aOnSucceeded, aOnError, aDone) {
    aClient.query(
      aQuery,aValues,
      function (queryErr, result) {
        if (aDone) {
          aDone(); // call `done()` to release the client back to the pool
        }
        if (queryErr) {
          NI.sendError(aOnError,queryErr,'PGSQL Query Error');
          return;
        }
        if (aOnSucceeded) {
          try {
            aOnSucceeded(result);
          }
          catch (err) {
            NI.sendError(aOnError,err,'PGSQL Handler Exception');
            return;
          }
        }
      }
    );
  }

  return {
    query: function(aQuery, aValues, aOnSucceeded, aOnError) {
      PGSQL.connect(conConfig, function(conErr, client, done) {
        if (conErr) {
          NI.sendError(aOnError,conErr,'Connection Error');
          return;
        }
        doQuery(client,aQuery,aValues,aOnSucceeded,aOnError,done);
      });
    },

    buildFunctionCall: function(aFunction, aParams) {
      var ns = aFunction.ns;
      if (NI.isEmpty(ns)) {
        ns = conConfig.database + ".public.";
      }
      else {
        ns += ".";
      }
      var r = "SELECT * FROM " + ns + aFunction.name + "(";
      if (aParams && aParams.length) {
        var i, ni
        var numParams = aParams.length;
        for (i = 0; i < numParams; ++i) {
          ni = i+1
          r += "$" + ni
          if (ni != numParams) {
            r += ","
          }
        }
      }
      r += ");";
      return r;
    },

    handleFunctionCallResult: function(aFunction, aResult, aOnSucceeded, aOnError) {
      try {
        // NI.print("... PGSQL handleFunctionCallResult: %s, %K", aFunction.name, aResult);
        if (!aResult || !aResult.rows || !aResult.rows[0]) {
          NI.sendError(
            aOnError,
            NI.format("PGSQL '%s': No Rows in the result.",
                      aFunction.name));
        }
        else if (aOnSucceeded) {
          // NI.println("... aResult: %K", aResult);
          var fn = aFunction.name;
          var rows = aResult.rows;
          if (aResult.fields.length === 1 && aResult.fields[0].name === fn) {
            //
            // If the function returns an array of JSON objects of the form:
            // [ { function_name: { key: "value" } },
            //   ... ]
            // then we set those values as row values.
            //
            // This allows us to write SP in PG that return a SETOF JSON which
            // in general is neater and simpler to build.
            //
            var numRows = rows.length;
            if (numRows > 0 &&
                (typeof(rows[0]) === "object" &&
                 (NI.isArray(rows[0][fn]) ||
                  NI.isObject(rows[0][fn]))))
            {
              for (var ri = 0; ri < numRows; ++ri) {
                if (__DEV__) {
                  NI.invariant(
                    (typeof(rows[ri]) === "object" &&
                     (NI.isArray(rows[ri][fn]) ||
                      NI.isObject(rows[ri][fn]))
                    ),
                    "rows[%d][%s] doesn't contain valid json: '%j'",
                    ri, fn, rows[ri][fn]);
                }
                rows[ri] = rows[ri][fn];
              }
            }
          }
          aOnSucceeded(rows);
        }
      }
      catch (exc) {
        NI.sendError(
          aOnError,exc,
          NI.format("PGSQL '%s': Function Result Exception",
                    aFunction.name));
        return;
      }
    },
  }
};

exports.createDriver = createDriverPG;
