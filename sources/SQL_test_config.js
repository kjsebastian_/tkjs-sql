var NI = require('tkjs-ni');

//
// Default databases, for testing. Set as single connection is what we want
// for testing so that all the queries are 'in order'.
//

exports.mysql_for_safe_tests = Object.freeze({
  driver: 'mysql',
  connectionLimit: 1,
  multipleStatements: true,
  host: NI.getEnvConfig("TESTDB_MYSQL_HOST"),
  database: NI.getEnvConfig("TESTDB_MYSQL_DB"),
  user: NI.getEnvConfig("TESTDB_MYSQL_USER"),
  password: NI.getEnvConfig("TESTDB_MYSQL_PWD"),
});

exports.pgsql_for_safe_tests = Object.freeze({
  driver: 'pgsql',
  connectionLimit: 1,
  host: NI.getEnvConfig("TESTDB_PGSQL_HOST"),
  database: NI.getEnvConfig("TESTDB_PGSQL_DB"),
  user: NI.getEnvConfig("TESTDB_PGSQL_USER"),
  password: NI.getEnvConfig("TESTDB_PGSQL_PWD"),
});

exports.mssql_for_safe_tests = Object.freeze({
  driver: 'mssql',
  connectionLimit: 1,
  multipleStatements: true,
  host: NI.getEnvConfig("TESTDB_MSSQL_HOST"),
  database: NI.getEnvConfig("TESTDB_MSSQL_DB"),
  user: NI.getEnvConfig("TESTDB_MSSQL_USER"),
  password: NI.getEnvConfig("TESTDB_MSSQL_PWD"),
});
