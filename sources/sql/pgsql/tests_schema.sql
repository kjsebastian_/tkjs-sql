CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- Test data
DROP TABLE IF EXISTS tests;
CREATE TABLE tests (
  id         SERIAL PRIMARY KEY               NOT NULL,
  parent_id  INT,
  type       VARCHAR(64),
  uuid       UUID DEFAULT uuid_generate_v4()  NOT NULL,
  created_tm TIMESTAMPTZ DEFAULT now()        NOT NULL,
  level      INT DEFAULT 10                   NOT NULL,
  label_txt  VARCHAR(1024) DEFAULT 'no label' NOT NULL,
  desc_obj   JSONB
);
CREATE UNIQUE INDEX tests_uuid_key ON tests (uuid);
CREATE INDEX tests_parent_id_index ON tests (parent_id);
CREATE INDEX tests_type_index ON tests (type);

INSERT INTO tests (id, parent_id, type, uuid, created_tm, level, label_txt, desc_obj)
VALUES (3, NULL, NULL, '776e6d44-8378-4419-aee3-f57a3173a171', '2015-06-09 12:36:59.514714', 10, 'another job', NULL);
INSERT INTO tests (id, parent_id, type, uuid, created_tm, level, label_txt, desc_obj) VALUES
  (4, NULL, NULL, 'c35765d2-213b-41f6-8e76-4bc776a92d35', '2015-06-09 12:36:59.514714', 10, 'deliver stuff to foo',
   NULL);
INSERT INTO tests (id, parent_id, type, uuid, created_tm, level, label_txt, desc_obj)
VALUES (5, NULL, NULL, '30ae013d-4ee5-47d4-aa12-a7719048432d', '2015-06-09 12:36:59.514714', 10, 'make a cake', NULL);
INSERT INTO tests (id, parent_id, type, uuid, created_tm, level, label_txt, desc_obj) VALUES
  (6, NULL, NULL, 'e20a1514-58e7-41be-8254-1a40529af1c3', '2015-06-09 12:36:59.514714', 10,
   'go fetch stuff from seller', NULL);
INSERT INTO tests (id, parent_id, type, uuid, created_tm, level, label_txt, desc_obj)
VALUES (7, NULL, NULL, 'dd57edb7-8af1-42a0-a82a-edd0fb935081', '2015-06-09 12:36:59.514714', 10, 'Yes job', NULL);
INSERT INTO tests (id, parent_id, type, uuid, created_tm, level, label_txt, desc_obj)
VALUES (8, NULL, NULL, 'd27b833f-1877-4de5-9480-34b9b7e9ac64', '2015-06-09 12:36:59.514714', 10, 'Job Label', NULL);
INSERT INTO tests (id, parent_id, type, uuid, created_tm, level, label_txt, desc_obj)
VALUES (9, NULL, NULL, '8af03773-5533-4d83-9b7a-a273291f5b1c', '2015-06-09 12:36:59.514714', 10, 'Job2 Label', NULL);
INSERT INTO tests (id, parent_id, type, uuid, created_tm, level, label_txt, desc_obj)
VALUES (10, NULL, NULL, 'f2d3ef69-3eec-4341-ab04-a2a7838015a2', '2015-06-09 12:36:59.514714', 10, 'Job3 Label', NULL);
INSERT INTO tests (id, parent_id, type, uuid, created_tm, level, label_txt, desc_obj) VALUES
  (78, NULL, NULL, '14e8ae02-93c3-4344-a7cf-4febad356fd9', '2015-06-09 16:39:05.016119', 10, 'Another Parent', NULL);
INSERT INTO tests (id, parent_id, type, uuid, created_tm, level, label_txt, desc_obj)
VALUES (79, 78, NULL, '9201843a-40c5-48c8-8d40-342b2bd80069', '2015-06-09 16:39:05.016119', 10, 'foo', NULL);
INSERT INTO tests (id, parent_id, type, uuid, created_tm, level, label_txt, desc_obj)
VALUES (80, 78, NULL, '777a492d-d97e-4f2e-b334-805aef53a1ba', '2015-06-09 16:39:05.016119', 10, 'bar', NULL);
INSERT INTO tests (id, parent_id, type, uuid, created_tm, level, label_txt, desc_obj)
VALUES (81, 78, NULL, '74fd6873-b814-4324-87d6-39e3c040843e', '2015-06-09 16:39:05.016119', 10, 'other task', NULL);
INSERT INTO tests (id, parent_id, type, uuid, created_tm, level, label_txt, desc_obj) VALUES
  (2, NULL, NULL, '53aaa51e-028c-40f6-b776-b7e584f7ad16', '2015-06-09 12:36:59.514714', 10, 'Second Foo Bar Set', NULL);
INSERT INTO tests (id, parent_id, type, uuid, created_tm, level, label_txt, desc_obj) VALUES
  (1, NULL, NULL, '9e7c9f78-099d-4b3e-b2e2-e90b1f680724', '2015-06-09 12:36:26.707173', 10, 'Test Call Label: 790039',
   NULL);
