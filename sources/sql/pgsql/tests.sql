-- Count the jobs
DROP FUNCTION IF EXISTS tests_count();
-- !{Export}
CREATE FUNCTION tests_count()
  RETURNS INT AS $func$
DECLARE
  rCount INT;
BEGIN
  SELECT COUNT(*)
  FROM tests
  INTO rCount;
  RETURN rCount;
END $func$ LANGUAGE plpgsql;

-- List the jobs
DROP FUNCTION IF EXISTS tests_list_ret_rows( INT );
DROP TYPE IF EXISTS tests_list_ret_t;
CREATE TYPE tests_list_ret_t AS (
  id        INT,
  level     INT,
  label_txt VARCHAR(1024)
);
-- !{Export}
CREATE FUNCTION tests_list_ret_rows(aLimit INT)
  RETURNS SETOF tests_list_ret_t AS $func$
BEGIN
  RETURN QUERY SELECT
     tests.id,
     tests.level,
     tests.label_txt
   FROM tests
   ORDER BY tests.id
               LIMIT aLimit;
END $func$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS tests_list_ret_json( INT );
-- !{Export}
CREATE FUNCTION tests_list_ret_json(aLimit INT)
  RETURNS SETOF JSON AS $func$
BEGIN
  RETURN QUERY WITH r AS
  (SELECT
     tests.id,
     tests.level,
     tests.label_txt
   FROM tests
   ORDER BY tests.id
   LIMIT aLimit)
  SELECT row_to_json(r.*)
  FROM r;
END $func$ LANGUAGE plpgsql;

-- Get a job from its id
DROP FUNCTION IF EXISTS tests_get_one( INT );
-- !{Export}
CREATE FUNCTION tests_get_one(aJobId INT)
  RETURNS SETOF tests AS $func$
BEGIN
  RETURN QUERY SELECT *
               FROM tests
               WHERE id = aJobId
               LIMIT 1;
END $func$ LANGUAGE plpgsql;

-- Sets the label of a particular job
DROP FUNCTION IF EXISTS tests_set_label( INT, TEXT );
-- !{Export}
CREATE FUNCTION tests_set_label(aJobId INT, aNewLabel TEXT)
  RETURNS INT AS $func$
DECLARE
  affectedRows INT;
BEGIN
  UPDATE tests
  SET label_txt = aNewLabel
  WHERE id = aJobId;
  GET DIAGNOSTICS affectedRows = ROW_COUNT;
  RETURN affectedRows;
END $func$ LANGUAGE plpgsql;
