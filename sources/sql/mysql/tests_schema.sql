-- List of drivers requests answers
DROP TABLE IF EXISTS tests;
CREATE TABLE tests (
  drivers_avail_req_sid CHAR(32)    NOT NULL,
  device_id             VARCHAR(16) NOT NULL,
  driver_hp             VARCHAR(32) NOT NULL,
  token_id              INT         NOT NULL, -- token_id = 0, request is locked
  avail_at_list         TEXT, -- NULL = 'not replied', [ { start_at: ..., end_at: ...}, ... ]
  updated_at            BIGINT,
  note_txt              TEXT,
  area_txt              VARCHAR(16), -- North, South, West, East, Central
  desc_txt              VARCHAR(100), -- Driver's description (name) - copied here because its always needed and doing a join for this is a wast of the DB's resources
  status_txt            VARCHAR(16), -- Communication status: NULL, sending, failed, sent, replied
  PRIMARY KEY (drivers_avail_req_sid, driver_hp),
  UNIQUE KEY (device_id, token_id)
);

INSERT INTO tests (drivers_avail_req_sid, device_id, driver_hp, token_id, avail_at_list, updated_at, note_txt, area_txt, desc_txt, status_txt) VALUES ('z8fD45', '444719', '+6584407569', 233664, '{"req_sid":"z8fD45","driver_hp":"+6584407569","token_id":"233664","force_overwrite":"y","area":"west","mon":"time_9am_to_6pm","tue":"time_9am_to_10pm","wed":"none","thu":"none","fri":"none","sat":"none"}', 1432712561151, null, null, 'Pierre', 'status_738989');
INSERT INTO tests (drivers_avail_req_sid, device_id, driver_hp, token_id, avail_at_list, updated_at, note_txt, area_txt, desc_txt, status_txt) VALUES ('z8fD45', '456999', '+6590118773', 992938, null, null, null, null, 'Dolita', null);
INSERT INTO tests (drivers_avail_req_sid, device_id, driver_hp, token_id, avail_at_list, updated_at, note_txt, area_txt, desc_txt, status_txt) VALUES ('z8fD45', '369123', '+6596387796', 812565, null, null, null, null, 'Zan', null);
