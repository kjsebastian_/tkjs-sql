var NI = require('tkjs-ni');
var SQL_STRING = require('./SQL_string');

function createDriverMssql(aInitConfig)/*: tSQLDriver */ {
  var MSSQL = require('node-mssql').mssql;
  NI.assert.has(aInitConfig,'host');
  NI.assert.has(aInitConfig,'user');
  NI.assert.has(aInitConfig,'password');
  NI.assert.has(aInitConfig,'database');

  var conConfig = {
    server: aInitConfig.host,
    user: aInitConfig.user,
    password: aInitConfig.password,
    database: aInitConfig.database,
    pool: {
      max: aInitConfig.connectionLimit || 10
    },
    options: {
      encrypt: true
    }
  };

  function doQuery(aClient, aQuery, aValues, aOnSucceeded, aOnError) {
    var request = aClient.request();
    request.multiple = true;
    var formatedQuery = SQL_STRING.format(aQuery,aValues);
    // NI.print("... MSSQL query: %s", formatedQuery);
    request.query(
      formatedQuery,
      function (queryErr, result, returnValue) {
        if (queryErr) {
          NI.sendError(aOnError,"[" + aQuery + "]: " + queryErr,'MSSQL Query Error: ');
          return;
        }
        if (aOnSucceeded) {
          try {
            // NI.print("... MSSQL query result: %K: %K", result, returnValue);
            aOnSucceeded(result);
          }
          catch (err) {
            NI.sendError(aOnError,err,'MSSQL Handler Exception');
            return;
          }
        }
      }
    );
  }

  var connection;

  return {
    query: function(aQuery, aValues, aOnSucceeded, aOnError) {
      if (!connection) {
        connection = new MSSQL.Connection(conConfig, function(conErr) {
          if (conErr) {
            NI.sendError(aOnError,conErr,'Connection Error');
            return;
          }
          doQuery(connection,aQuery,aValues,aOnSucceeded,aOnError);
        });
      }
      else {
        doQuery(connection,aQuery,aValues,aOnSucceeded,aOnError);
      }
    },

    buildFunctionCall: function(aFunction, aParams) {
      var ns = aFunction.ns;
      if (!NI.isEmpty(ns)) {
        ns += ".";
      }
      var r = "EXECUTE " + ns + aFunction.name + " ";
      if (aParams && aParams.length) {
        var numParams = aParams.length, paramDef;
        for (var i = 0; i < numParams; ++i) {
          r += "?"
          if ((i+1) != numParams) {
            r += ","
          }
          // process parameter types
          paramDef = aFunction.params[i];
          if (paramDef.type == "json") {
            // convert json to text
            aParams[i] = JSON.stringify(aParams[i]);
          }
        }
      }
      r += ";";
      return r;
    },

    handleFunctionCallResult: function(aFunction, aResult, aOnSucceeded, aOnError) {
      try {
        // NI.print("... MSSQL handleFunctionCallResult: %s, %K", aFunction.name, aResult);
        if (!aResult || !aResult[0]) {
          NI.sendError(
            aOnError,
            NI.format("MSSQL '%s': No Rows in the result.",
                      aFunction.name));
        }
        else if (aOnSucceeded) {
          var res = aResult[0];
          for (var i = aResult.length; i >= 1; --i) {
            res = res.concat(aResult[i]);
          }
          aOnSucceeded(res);
        }
      }
      catch (exc) {
        NI.sendError(
          aOnError,exc,
          NI.format("MSSQL '%s': Function Result Exception",
                    aFunction.name));
        return;
      }
    },
  }
};
exports.createDriver = createDriverMssql;
