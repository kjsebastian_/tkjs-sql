/* global FIXTURE, TEST_ASYNC, STARTUP, SHUTDOWN */
var NI = require('tkjs-ni');
var SQL = require('./SQL');

FIXTURE("PGSQL", function() {
  var PGSQL;
  STARTUP(function(done) {
    this.timeout(30000);

    PGSQL = SQL.init("pgsql_for_safe_tests");
    PGSQL.registerFunctions('sources/sql/pgsql/**/*.sql', NI.log);

    // wait for the db to be connected
    NI.println("... Waiting for DB connection to '%s' ...", PGSQL.config.host);
    PGSQL.waitForConnection(function() {
      done();
      NI.println("... done ...");
    });
  });

  SHUTDOWN(function() {
  });

  TEST_ASYNC("call the function's code directly", function(done) {
    PGSQL.query(
      [
        "SELECT tests.id, tests.level, tests.label_txt FROM tests LIMIT $1;"
      ], [3],
      function(aRes) {
        NI.println("tests_list -> %K", aRes);
        done();
      },
      function(err) {
        throw done(err);
      });
  });

  TEST_ASYNC("funcall test count", function(done) {
    PGSQL.call("tests_count", undefined, function(aRes) {
      NI.println("tests_count -> %K", aRes);
      done();
    }, function(err) {
      throw done(err);
    });
  });

  TEST_ASYNC("funcall list ret rows", function(done) {
    PGSQL.call("tests_list_ret_rows", [3], function(aRes) {
      NI.println("tests_list_ret_rows -> %K", aRes);
      NI.assert.isArray(aRes);
      NI.assert.equals(3,aRes.length);
      NI.assert.equals(10,aRes[0].level);
      NI.assert.equals(10,aRes[1].level);
      NI.assert.equals(10,aRes[2].level);
      done();
    }, function(err) {
      throw done(err);
    });
  });

  TEST_ASYNC("funcall list ret json", function(done) {
    PGSQL.call("tests_list_ret_json", [3], function(aRes) {
      NI.println("tests_list_ret_json -> %K", aRes);
      NI.assert.isArray(aRes);
      NI.assert.equals(3,aRes.length);
      NI.assert.equals(10,aRes[0].level);
      NI.assert.equals(10,aRes[1].level);
      NI.assert.equals(10,aRes[2].level);
      done();
    }, function(err) {
      throw done(err);
    });
  });

  TEST_ASYNC("funcall set / get", function(done) {
    var randNumber = ""+Math.floor(Math.random()*1e6);
    var newLabel = "Test Call Label: "+randNumber;
    PGSQL.call("tests_set_label", [1,newLabel], function(aResSet) {
      NI.println("tests_set_label -> %K", aResSet);
      NI.assert.equals(1, aResSet[0].tests_set_label);
      PGSQL.call("tests_get_one", [1], function(aResGet) {
        NI.println("tests_get_one -> %K", aResGet);
        NI.assert.equals(newLabel, aResGet[0].label_txt);
        done();
      }, function(err) {
        throw done(err);
      });
    }, function(err) {
      throw done(err);
    });
  });

  TEST_ASYNC("funcall set_invalid_id", function(done) {
    var randNumber = ""+Math.floor(Math.random()*1e6);
    var newLabel = "Test Call Label: "+randNumber;
    PGSQL.call("tests_set_label", [123456789,newLabel], function(aResSet) {
      NI.println("tests_set_label invalid_id -> %K", aResSet);
      NI.assert.strictlyEqual(0, aResSet[0].tests_set_label);
      done();
    }, function(err) {
      throw done(err);
    });
  });

});
