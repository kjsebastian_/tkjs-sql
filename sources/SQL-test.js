/* global FIXTURE, TEST */
var NI = require('tkjs-ni');
var SQL = require('./SQL');
var println = NI.print;

var testFuncDefs = [
  {
    str: "CREATE FUNCTION foobar()",
    name: "foobar",
    numParams: 0
  },
  {
    str: "CREATE FUNCTION jobs_insert_parent_and_children(aParentLabel TEXT, aChildrenLabels TEXT [])",
    name: "jobs_insert_parent_and_children",
    numParams: 2,
  },
  {
    str: "CREATE PROCEDURE somedb.cart_get_items(IN aLimit INT)",
    ns: "somedb",
    name: "cart_get_items",
    numParams: 1
  },
  {
    str: "CREATE FUNCTION jobs_list(aLimit INT, IN aNarf TEXT) RETURNS jobs AS $func$",
    name: "jobs_list",
    numParams: 2
  },
  {
    str: ["ALTER PROCEDURE tests_set_status(@DeviceId  INT,",
          "                                 @NewStatus VARCHAR(16)) -- multiline on purpose"].join("\n"),
    name: "tests_set_status",
    numParams: 2
  }
]

FIXTURE("SQL", function() {

  TEST("parseFunction", function() {
    NI.forEach(testFuncDefs, function(testFunc) {
      println("--- parsing: %s", testFunc.name);
      var funcDef = SQL.parseFunction(testFunc.str);
      NI.assert.equals(testFunc.name, funcDef.name);
      if (testFunc.ns) {
        NI.assert.equals(testFunc.ns, funcDef.ns);
      }
      NI.assert.equals(testFunc.numParams, funcDef.params.length);
      println("func: %K", funcDef);
    });
  });

  TEST("parseDefs_mysql", function() {
    var defs = SQL.parseSql('./sources/sql/mysql/tests.sql');
    println("defs: %K", defs);
  });

  TEST("escape", function() {
    var r;

    r = SQL.escape("foo");
    NI.println("r: " + r);
    NI.assert.equals(
      "'foo'",
      r)

    r = SQL.escapeId("foo");
    NI.println("r: " + r);
    NI.assert.equals(
      "`foo`",
      r)

    r = SQL.format("SELECT * FROM ?? WHERE foo=? LIMIT ?;",["table","foo'bar",123]);
    NI.println("r: " + r);
    NI.assert.equals(
      "SELECT * FROM `table` WHERE foo='foo\\'bar' LIMIT 123;",
      r)

    r = SQL.format("SELECT * FROM table WHERE x='y';");
    NI.println("r: " + r);
    NI.assert.equals(
      "SELECT * FROM table WHERE x='y';",
      r)
  });
});
