/* global FIXTURE, TEST_ASYNC, STARTUP, SHUTDOWN */
var NI = require('tkjs-ni');
var SQL = require('./SQL');
var GLOB = require('glob');
var println = NI.print;

FIXTURE("MYSQL", function() {
  var MYSQL;

  var sources = GLOB.sync('sources/sql/mysql/**/*.sql');
  STARTUP(function(done) {
    MYSQL = SQL.init('mysql_for_safe_tests');

    NI.forEach(sources, function(aSrc) {
      println("- MySQL source: %s", aSrc);
      var defs = SQL.parseSql(aSrc);
      var funcs = NI.selectn('functions', defs);
      if (!NI.isEmpty(funcs)) {
        println("  functions: %j", NI.map(funcs,function(f) {
          MYSQL.registerFunction(f);
          return f.name
        }));
      }
    });

    // NI.forEach(MYSQL.functions, function(aFunc) {
      // println("  func: %j", aFunc);
    // });

    // wait for the db to be connected
    println("... Waiting for MySQL connection to '%s' ...", MYSQL.config.host);
    MYSQL.waitForConnection(function() {
      done();
      println("... done ...");
    });
  });

  SHUTDOWN(function() {
  });

  TEST_ASYNC("call the function's code directly", function(done) {
    MYSQL.query(
      [
        "SELECT COUNT(*) as num_test_entries FROM tests;",
        "SELECT device_id, desc_txt, status_txt FROM tests WHERE drivers_avail_req_sid = 'z8fD45' LIMIT ?;"
      ], [3],
      function(aRes) {
        println("tests_list (sql code) -> %K", aRes);
        done();
      },
      function(err) {
        throw done(err);
      });
  });

  TEST_ASYNC("funcall list_and_count", function(done) {
    MYSQL.call("tests_list_and_count", [3], function(aRes) {
      println("tests_list_and_count -> %K", aRes);
      NI.assert.equals(3, aRes[0].num_test_entries);
      NI.assert.equals("Dolita", aRes[2].desc_txt);
      done();
    }, function(err) {
      throw done(err);
    });
  });

  TEST_ASYNC("funcall list", function(done) {
    MYSQL.call("tests_list", [3], function(aRes) {
      println("tests_list -> %K", aRes);
      NI.assert.equals("Dolita", aRes[1].desc_txt);
      done();
    }, function(err) {
      throw done(err);
    });
  });

  TEST_ASYNC("funcall set / get", function(done) {
    var randNumber = ""+Math.floor(Math.random()*1e6);
    var newStatus = "status_"+randNumber; // must be < 16 chars
    MYSQL.call("tests_set_status", [444719,newStatus], function(aResSet) {
      println("tests_set_label -> %K", aResSet);
      NI.assert.strictlyEqual(1, aResSet[0].tests_set_status);

      MYSQL.call("tests_get_one", [444719], function(aResGet) {
        println("tests_get_one -> %K", aResGet);
        NI.assert.equals(newStatus, aResGet[0].status_txt);
        done();
      }, function(err) {
        throw done(err);
      });
    }, function(err) {
      throw done(err);
    });
  });

  TEST_ASYNC("funcall set_invalid_id", function(done) {
    var randNumber = ""+Math.floor(Math.random()*1e6);
    var newStatus = "status_"+randNumber; // must be < 16 chars
    MYSQL.call("tests_set_status", [1,newStatus], function(aResSet) {
      println("tests_set_label invalid_id -> %K", aResSet);
      NI.assert.strictlyEqual(0, aResSet[0].tests_set_status);
      done();
    }, function(err) {
      throw done(err);
    });
  });

});
