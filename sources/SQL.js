/* global __DEV__ */
var NI = require('tkjs-ni'); // eslint-disable-line
var FS = require('fs');
var SQL_STRING = require('./SQL_string');
var GLOB = require('glob');

//==========================================================================
//  Utils
//==========================================================================
function prepareQuery(aQuery,aParams) {
  var r = "";
  if (NI.isArray(aQuery) && aQuery instanceof Array) {
    r = aQuery.join("\n");
  }
  else if (typeof(aQuery) === "string") {
    r = aQuery;
  }
  else {
    throw NI.Error("Invalid type of aQuery: " + typeof(aQuery));
  }
  if (NI.stringContains(r,"${")) {
    r = NI.stringReplaceVar(r,aParams||{});
  }
  // NI.log("QUERY:\n%s", query);
  NI.assert.stringNotEmpty(r);
  return r;
}

function parseParameter(aParamDecl,index,funcName) {
  aParamDecl = NI.stringTrim(aParamDecl);
  var tokens = aParamDecl.split(/\s+/);
  var currentToken = 0;
  if (tokens[currentToken] == "IN" || tokens[currentToken] == "OUT") {
    ++currentToken;
  }
  if (tokens.length-currentToken < 2) {
    throw NI.Error("%s: invalid parameter '%d' declaration: %s", funcName, index, aParamDecl);
  }

  var sqlTypeName = tokens.slice(currentToken+1).join(" ").toLowerCase();
  var sqlRawTypeName = sqlTypeName;
  switch (sqlTypeName) {
  case "bigint":
  case "int": {
    break;
  }
  case "double": {
    break;
  }
  case "citext":
  case "text": {
    break;
  }
  case "citext []":
  case "text []": {
    break;
  }
  case "uuid": {
    break;
  };
  case "varchar(2048)": // we use varchar(2048) for Json on MSSQL...
  case "mediumtext": // we use mediumtext for Json on MySQL...
  case "json":
  case "jsonb": {
    sqlTypeName = "json";
    break;
  }
  case "double precision": {
    sqlTypeName = "double";
    break;
  }
  default: {
    if (NI.stringStartsWith(sqlTypeName,"varchar(")) {
      sqlTypeName = "varchar";
    }
    else {
      throw NI.Error("%s: unknown parameter '%d' type: %s", funcName, index, sqlTypeName);
    }
  }
  }

  return {
    name: tokens[currentToken],
    type: sqlTypeName,
    rawType: sqlRawTypeName
  }
}

var regex_CREATE_FUNCTION = new RegExp("^(CREATE|ALTER)\\s+(FUNCTION|PROCEDURE)\\s+([\.a-zA-Z0-9_]+)");

function parseFunction(aFullFuncDecl) {
  // parse the function name
  var funcNameDecl = NI.stringBefore(aFullFuncDecl,"(");
  var funcNameMatches = funcNameDecl.match(regex_CREATE_FUNCTION);
  if (!funcNameMatches) {
    throw NI.Error("Expected function declaration in '%s'.", aFullFuncDecl);
  }
  // NI.log("... %K", funcNameMatches);
  if (funcNameMatches.length != 4) {
    throw NI.Error("Invalid function declaration format in '%s'.", aFullFuncDecl);
  }
  var funcKind = funcNameMatches[2];
  if (NI.isEmpty(funcKind)) {
    throw NI.Error("Cant parse the function kind in '%s'.", aFullFuncDecl);
  }
  var funcName = funcNameMatches[3];
  if (NI.isEmpty(funcKind)) {
    throw NI.Error("Cant parse the function name in '%s'.", aFullFuncDecl);
  }

  // parse the function namespace
  var nsName = "";
  if (NI.stringContains(funcName,".")) {
    nsName = NI.stringRBefore(funcName,".");
    funcName = NI.stringRAfter(funcName,".");
  }

  var funcParamsDecl = NI.stringRBefore(NI.stringAfter(aFullFuncDecl,"("),")");
  if (typeof(funcParamsDecl) !== "string") {
    throw NI.Error("Cant parse the function params in '%s'.", aFullFuncDecl);
  }

  var params;
  var paramsDecl = NI.stringTrim(funcParamsDecl);
  if (NI.isEmpty(paramsDecl)) {
    params = []
  }
  else {
    params = NI.map(paramsDecl.split(','), function(param,index) {
      return parseParameter(param,index,funcName);
    })
  }

  return {
    name: funcName,
    ns: nsName,
    params: params,
    kind: funcKind,
  };
}
exports.parseFunction = parseFunction;

function parseSql(aFile,aContent) {
  var functions = [];
  var content = aContent || FS.readFileSync(aFile, {encoding: 'utf-8'}).toString();
  var lines = content.split('\n'), line;
  var numLines = lines.length;
  var lineIndex = 0;

  function getNextLine() {
    ++lineIndex;
    line = lines[lineIndex];
    // strip line comments
    if (NI.stringContains(line,"--")) {
      line = NI.stringRBefore(line,"--");
    }
    line = NI.stringTrim(line);
    return line;
  }

  try {
    for ( ;lineIndex < numLines; ) {
      line = NI.stringTrim(lines[lineIndex]);
      if (NI.stringStartsWith(line, "-- !") ||
          NI.stringStartsWith(line, "--! "))
      {
        line = NI.stringTrim(line.slice(4)).toLowerCase();
        if (line != "{export}") {
          // for now we only support {export}, yey ;)
          throw NI.Error("Unknown sql definition attribute: '%s'", line);
        }

        // expect the function declaration right after
        line = getNextLine();
        // If ends with a comma we grab the next line, implying a multiline
        // params decl. If ends with a '\' we also grab the next line,
        // explicit continuation.
        while ((lineIndex < numLines) &&
               (NI.stringEndsWith(line,",") ||
                NI.stringEndsWith(line,"\\"))
              )
        {
          line += getNextLine();
        }

        var f = parseFunction(line);
        f.file = aFile;
        functions.push(f);
      }
      ++lineIndex;
    }

    return {
      functions: functions
    }
  }
  catch (err) {
    throw NI.Error("%s:%d: Parsing Error: %s",
                   aFile, lineIndex+1, err.stack);
  }
}
exports.parseSql = parseSql;

//==========================================================================
//  High level API
//==========================================================================
var _drivers = {};

function registerDriver(aName, aDriver) {
  NI.assert.hasnt(_drivers,aName); // don't double register
  _drivers[aName] = aDriver;
}
exports.registerDriver = registerDriver;

registerDriver('pgsql', require('./SQL_pgsql'));
registerDriver('mysql', require('./SQL_mysql'));
registerDriver('mssql', require('./SQL_mssql'));

function createDriverFromName(aDriverName, aConfig) {
  if (!(aDriverName in _drivers)) {
    throw NI.Error("Unknown SQL driver '%s'.", aDriverName);
  }
  return _drivers[aDriverName].createDriver(aConfig);
}

var _dbConfigs = undefined;
function loadConfig(aConfigFile) {
  _dbConfigs = require(aConfigFile || './SQL_test_config.js');
  return exports;
}
exports.loadConfig = loadConfig;

function init(aConfigName)  {
  NI.assert.stringNotEmpty(aConfigName);
  if (!_dbConfigs) {
    loadConfig();
  }

  var config = _dbConfigs[aConfigName];
  if (!config) {
    throw NI.Error("Couldn't find database config '%s'.", aConfigName);
  }

  // clone because we could modify the config and it should be frozen
  config = NI.assignClone(config);
  NI.assert.has(config,'driver');

  var driverName = config.driver;
  delete config.driver;

  var driver = createDriverFromName(driverName,config);
  function _query(aQuery, aValues, aOnSucceeded, aOnError, aParams) {
    try {
      NI.invariant(
        aValues === undefined || NI.isArray(aValues),
        "aValues: Query parameter values should be an array or undefined.");
      NI.invariant(
        aOnSucceeded === undefined || NI.isFunction(aOnSucceeded),
        "aOnSucceeded: A valid callback must be specified.");
      NI.invariant(
        aOnError === undefined || NI.isFunction(aOnError) || NI.hasFunction(aOnError,'send'),
        "aOnError: A valid callback must be specified.");
      NI.invariant(
        aParams === undefined || NI.isObject(aParams),
        "aParams: Should be an object or undefined.");

      var preparedQuery = prepareQuery(aQuery,aParams);
      return driver.query(
        preparedQuery,
        aValues||[], // most drivers don't like an undefined aValues
        aOnSucceeded,
        aOnError,
        aParams);
    }
    catch (err) {
      NI.sendError(aOnError,err,'query');
      return undefined;
    }
  }

  function _queryResult(aQuery,aValues,aOnSucceeded,aOnError,aParams) {
    return _query(
      aQuery,
      aValues,
      function(result) {
        if (!result || !result.rows || !result.rows[0]) {
          NI.sendError(aOnError,'No Result.');
        }
        else {
          aOnSucceeded(result);
        }
      },
      aOnError,
      aParams);
  };

  var functions = {}
  function registerFunction(aFunction) {
    NI.assert.has(aFunction,'kind');
    NI.assert.has(aFunction,'ns');
    NI.assert.has(aFunction,'name');
    NI.assert.has(aFunction,'params');
    if (aFunction.name in functions) {
      throw NI.Error("SQL function '%s' already registered.", aFunction.name);
    }
    functions[aFunction.name] = aFunction;
  }

  function registerFunctions(aGLOB) {
    var sources = GLOB.sync(aGLOB);
    if (config.logSql) {
      config.logSql(NI.format("Register SQL functions from: %s", aGLOB));
    }
    NI.forEach(sources, function(aSrc) {
      var defs = parseSql(aSrc);
      var funcs = NI.selectn('functions', defs);
      if (!NI.isEmpty(funcs)) {
        var registered = NI.map(funcs,function(f) {
          registerFunction(f);
          return f.name;
        })
        if (config.logSql) {
          config.logSql(NI.format(
            "SQL %s functions in '%s': %j",
            driverName,
            aSrc,
            registered));
        }
      }
      else {
        if (config.logSql) {
          config.logSql(NI.format(
            "SQL %s no exported function in '%s'.",
            driverName,
            aSrc));
        }
      }
    });
  }

  function getFunction(aFunctionName, aParams, aOnError) {
    var func = functions[aFunctionName];
    if (!func) {
      NI.sendError(aOnError,NI.Error("Unknown SQL function '%s'", aFunctionName));
      return undefined;
    }
    var numPassedParams = aParams ? aParams.length : 0;
    var numFuncParams = func.params ? func.params.length : 0;
    if (numPassedParams != numFuncParams) {
      NI.sendError(aOnError,NI.Error("Calling '%s', invalid number of parameters '%d', expected '%d'.", aFunctionName, numPassedParams, numFuncParams));
      return undefined;
    }
    return func;
  }

  function call(aFunctionName, aParams, aOnSucceeded, aOnError) {
    var func = getFunction(aFunctionName, aParams, aOnError);
    var callQuery = driver.buildFunctionCall(func,aParams);
    // NI.print("... callQuery: %s", callQuery);
    return _query(
      callQuery,
      aParams,
      function(aResult) {
        driver.handleFunctionCallResult(func, aResult, aOnSucceeded, aOnError);
      },
      aOnError);
  }

  function callPromise(aFunctionName, aParams) {
    return NI.Promise(function(aResolve,aReject) {
      call(aFunctionName, aParams, aResolve, aReject);
    });
  }

  function callMakeFunc(aFunctionName,aReturnSingle) {
    var func = functions[aFunctionName];
    if (!func) {
      throw NI.Error("Unknown SQL function '%s'", aFunctionName);
    }
    return function() {
      // Using a for loop because it gets optimised by V8 by Array.slice doesnt.
      // var args = Array.prototype.slice.call(arguments);
      var argsCount = arguments.length;
      var args = new Array(argsCount);
      for(var argsI = 0; argsI < argsCount; ++argsI) {
        args[argsI] = arguments[argsI];
      }
      return callPromise(aFunctionName, args).then(function(aDBReply) {
        return aReturnSingle ? aDBReply[0] : aDBReply;
      });
    }
  }

  function waitForConnection(aOnConnected) {
    driver.query("SELECT 1;", undefined, aOnConnected, aOnConnected);
  }

  return {
    config: config,
    driver: driver,
    query: _query,
    queryResult: _queryResult,
    escape: SQL_STRING.escape,
    escapeId: SQL_STRING.escapeId,
    format: SQL_STRING.format,
    registerFunction: registerFunction,
    registerFunctions: registerFunctions,
    waitForConnection: waitForConnection,
    functions: functions,
    getFunction: getFunction,
    call: call,
    callPromise: callPromise,
    callMakeFunc: callMakeFunc
  };
};
exports.init = init;

exports.escape = SQL_STRING.escape;
exports.escapeId = SQL_STRING.escapeId;
exports.format = SQL_STRING.format;
