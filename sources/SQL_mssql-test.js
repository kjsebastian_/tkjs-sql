/* global FIXTURE, TEST_ASYNC, STARTUP, SHUTDOWN */
var NI = require('tkjs-ni');
var SQL = require('./SQL');
var GLOB = require('glob');
var println = NI.print;

FIXTURE("MSSQL", function() {
  this.timeout(30000);

  var MSSQL;

  var sources = GLOB.sync('sources/sql/mssql/**/*.sql');
  STARTUP(function(done) {
    MSSQL = SQL.init('mssql_for_safe_tests');

    NI.forEach(sources, function(aSrc) {
      println("- Mssql source: %s", aSrc);
      var defs = SQL.parseSql(aSrc);
      var funcs = NI.selectn('functions', defs);
      if (!NI.isEmpty(funcs)) {
        println("  functions: %K", NI.map(funcs,function(f) {
          MSSQL.registerFunction(f);
          return f.name
        }));
      }
    });

    // NI.forEach(MSSQL.functions, function(aFunc) {
      // println("  func: %j", aFunc);
    // });

    // wait for the db to be connected
    println("... Waiting for Mssql connection to '%s' ...", MSSQL.config.host);
    MSSQL.waitForConnection(function() {
      done();
      println("... done ...");
    });
  });

  SHUTDOWN(function() {
  });

  TEST_ASYNC("call the function's code directly", function(done) {
    MSSQL.query(
      [
        "SELECT COUNT(*) as num_test_entries FROM tests;",
        "SELECT TOP ? device_id, desc_txt, status_txt FROM tests WHERE drivers_avail_req_sid = 'z8fD45';"
      ], [3],
      function(aRes) {
        println("tests_list (sql code) -> %K", aRes);
        done();
      },
      function(err) {
        throw done(err);
      });
  });

  TEST_ASYNC("funcall list_and_count", function(done) {
    MSSQL.call("tests_list_and_count", [3], function(aRes) {
      println("tests_list_and_count -> %K", aRes);
      NI.assert.equals(3, aRes[0].num_test_entries);
      NI.assert.equals("Dolita", aRes[2].desc_txt);
      done();
    }, function(err) {
      throw done(err);
    });
  });

  TEST_ASYNC("funcall list", function(done) {
    MSSQL.call("tests_list", [3], function(aRes) {
      println("tests_list -> %K", aRes);
      NI.assert.equals("Dolita", aRes[1].desc_txt);
      done();
    }, function(err) {
      throw done(err);
    });
  });

  TEST_ASYNC("funcall set / get", function(done) {
    var randNumber = ""+Math.floor(Math.random()*1e6);
    var newStatus = "status_"+randNumber; // must be < 16 chars
    MSSQL.call("tests_set_status", [444719,newStatus], function(aResSet) {
      println("tests_set_label -> %K", aResSet);
      NI.assert.strictlyEqual(1, aResSet[0].tests_set_status);

      MSSQL.call("tests_get_one", [444719], function(aResGet) {
        println("tests_get_one -> %K", aResGet);
        NI.assert.equals(newStatus, aResGet[0].status_txt);
        done();
      }, function(err) {
        throw done(err);
      });
    }, function(err) {
      throw done(err);
    });
  });

  TEST_ASYNC("funcall set_invalid_id", function(done) {
    var randNumber = ""+Math.floor(Math.random()*1e6);
    var newStatus = "status_"+randNumber; // must be < 16 chars
    MSSQL.call("tests_set_status", [1,newStatus], function(aResSet) {
      println("tests_set_label invalid_id -> %K", aResSet);
      NI.assert.strictlyEqual(0, aResSet[0].tests_set_status);
      done();
    }, function(err) {
      throw done(err);
    });
  });

});
